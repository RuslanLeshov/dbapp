package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.Customer;
import ru.nsu.ccfit.database.leshov.services.CustomerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    public List<Customer> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Customer add(@RequestBody @Valid Customer customer) {
        return service.add(customer);
    }

    @GetMapping("/filter")
    public List<Customer> filter(@RequestParam(required = false) String filter) {
        if (filter == null) {
            return getAll();
        }

        return service.filterByProductName(filter);
    }

    @GetMapping("/active")
    public List<Customer> sortByActivity() {
        return service.findMostActive();
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable("id") Customer savedCustomer, @RequestBody @Valid Customer newCustomer) {
        return service.update(savedCustomer, newCustomer);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable("id") Customer customer) {
        service.delete(customer);
    }
}
