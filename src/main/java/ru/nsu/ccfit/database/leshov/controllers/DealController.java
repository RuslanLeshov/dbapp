package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.Deal;
import ru.nsu.ccfit.database.leshov.domain.DealParty;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.services.DealService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/deal")
public class DealController {
    private final DealService service;

    public DealController(DealService service) {
        this.service = service;
    }

    @GetMapping
    public Page<Deal> getAll(@RequestParam(name = "outlet_id", required = false) Outlet outlet,
                             @RequestParam(name = "filter", required = false) String dealTypeFilter,
                             @RequestParam(name = "dateFrom", required = false) String dateFrom,
                             @RequestParam(name = "dateTo", required = false) String dateTo,
                             @RequestParam(name = "seller_id", required = false) Long sellerId,
                             @RequestParam(name = "buyer_id", required = false) Long buyerId,
                             @RequestParam(required = false, defaultValue = "5") Integer size,
                             @RequestParam(required = false, defaultValue = "0") Integer page,
                             @RequestParam String sortParam, @RequestParam String sortOrder
    ) {
        Sort sort = Sort.by(sortParam);
        if (sortOrder.equals("desc")) {
            sort = sort.descending();
        }
        else {
            sort = sort.ascending();
        }
        PageRequest pageable = PageRequest.of(page, size, sort);
        if (sellerId != null) {
            return service.getAllBySellerId(sellerId, pageable);
        }

        if (buyerId != null) {
            return service.getAllByBuyerId(buyerId, pageable);
        }
        return service.getAll(outlet, dealTypeFilter, dateFrom, dateTo, pageable);
    }

    @GetMapping("/income")
    public Double getIncome(@RequestParam(name = "outlet_id", required = false) Outlet outlet) {
        if (outlet == null) {
            return service.getTotalIncome();
        }
        return service.getIncome(outlet);
    }

    @GetMapping("/expense")
    public Double getExpense(@RequestParam(name = "outlet_id", required = false) Outlet outlet) {
        if (outlet == null) {
            return service.getTotalExpense();
        }
        return service.getExpense(outlet);
    }

    @GetMapping("/products")
    public List<Product> getDealProducts(@RequestParam(name = "deal_id") Deal deal) {
        return deal.getProducts();
    }

    @PostMapping
    public Deal add(@RequestBody @Valid List<Product> products,
                    @RequestParam(name = "seller_id") DealParty seller,
                    @RequestParam(name = "buyer_id") DealParty buyer,
                    @RequestParam(name = "outlet_id", required = false) Outlet outlet) {
        return service.add(products, seller, buyer, outlet);
    }
}
