package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.DealParty;

@RestController
@RequestMapping("/dp")
public class DealPartyController {
    @GetMapping("/{id}")
    public DealParty get(@PathVariable(name = "id") DealParty dp) {
        return dp;
    }

}
