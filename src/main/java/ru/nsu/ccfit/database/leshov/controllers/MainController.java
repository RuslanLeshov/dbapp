package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Storage;
import ru.nsu.ccfit.database.leshov.dtos.OutletDto;
import ru.nsu.ccfit.database.leshov.services.OutletService;
import ru.nsu.ccfit.database.leshov.services.ProductService;
import ru.nsu.ccfit.database.leshov.services.StorageManager;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class MainController {
    @Value("${spring.profiles.active:prod}")
    private String profile;

    private final OutletService outletService;


    public MainController(OutletService outletService) {
        this.outletService = outletService;
    }


    @GetMapping
    public String main(Model model) {
        HashMap<Object, Object> data = new HashMap<>();
        data.put("outlets", outletService.getAllOutlets()
                .stream()
                .map(OutletDto::new)
                .collect(Collectors.toList()));
        model.addAttribute("frontendData", data);
        model.addAttribute("isDevMode", "dev".equals(profile));
        return "index";
    }
}
