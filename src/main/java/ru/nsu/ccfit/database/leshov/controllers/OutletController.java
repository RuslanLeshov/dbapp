package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.DepartmentStore;
import ru.nsu.ccfit.database.leshov.domain.Kiosk;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Shop;
import ru.nsu.ccfit.database.leshov.dtos.OutletDto;
import ru.nsu.ccfit.database.leshov.services.OutletService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/outlet")
public class OutletController {
    private final OutletService outletService;

    public OutletController(OutletService outletService) {
        this.outletService = outletService;
    }

    @GetMapping
    public List<OutletDto> getAllOutlets() {
        return outletService.getAllOutlets()
                .stream()
                .map(OutletDto::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public OutletDto getOutletById(@PathVariable("id") Long id) {
        return new OutletDto(outletService.getOutlet(id));
    }

    @GetMapping("/output")
    public Double getOutput(@RequestParam("outlet_id") Outlet outlet) {
        return outletService.calculateOutput(outlet);
    }

    @GetMapping("/efficiency")
    public Double getEfficiency(@RequestParam("outlet_id") Outlet outlet) {
        return outletService.calculateEfficiency(outlet);
    }

    @GetMapping("/totalsalary")
    public Double getTotalSalary(@RequestParam("outlet_id") Outlet outlet) {
        return outletService.getTotalWorkersSalary(outlet);
    }

    @PostMapping
    public OutletDto addOutlet(@RequestBody @Valid Outlet outlet) {
        return new OutletDto(outletService.createOutlet(outlet));
    }

    @PostMapping("/shop")
    public OutletDto addShop(@RequestBody @Valid Shop outlet) {
        return new OutletDto(outletService.createOutlet(outlet));
    }

    @PostMapping("/deptstore")
    public OutletDto addDepartmentStore(@RequestBody @Valid DepartmentStore outlet) {
        return new OutletDto(outletService.createOutlet(outlet));
    }

    @PostMapping("/kiosk")
    public OutletDto addKiosk(@RequestBody @Valid Kiosk outlet) {
        return new OutletDto(outletService.createOutlet(outlet));
    }

    @PutMapping("/{id}")
    public OutletDto updateOutlet(@RequestBody @Valid Outlet outlet,
                                  @PathVariable(name = "id") Outlet storedOutlet) {
        return new OutletDto(outletService.updateOutlet(outlet, storedOutlet));
    }

    @PutMapping("/shop/{id}")
    public OutletDto updateShop(@RequestBody @Valid Shop outlet,
                                  @PathVariable(name = "id") Outlet storedOutlet) {
        return new OutletDto(outletService.updateOutlet(outlet, storedOutlet));
    }

    @PutMapping("/deptstore/{id}")
    public OutletDto updateDepartmentStore(@RequestBody @Valid DepartmentStore outlet,
                                  @PathVariable(name = "id") Outlet storedOutlet) {
        return new OutletDto(outletService.updateOutlet(outlet, storedOutlet));
    }

    @PutMapping("/kiosk/{id}")
    public OutletDto updateKiosk(@RequestBody @Valid Kiosk outlet,
                                  @PathVariable(name = "id") Outlet storedOutlet) {
        return new OutletDto(outletService.updateOutlet(outlet, storedOutlet));
    }


    @DeleteMapping("/{id}")
    public void deleteOutlet(@PathVariable(name = "id") Outlet outlet) {
        outletService.deleteOutlet(outlet);
    }
}
