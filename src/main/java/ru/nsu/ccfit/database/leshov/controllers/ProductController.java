package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.Deal;
import ru.nsu.ccfit.database.leshov.domain.DealParty;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Provider;
import ru.nsu.ccfit.database.leshov.services.ProductService;
import ru.nsu.ccfit.database.leshov.services.ProviderService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAll(@RequestParam(name = "owner_id") Long ownerId) {
        return ResponseEntity.ok(service.getAll(ownerId));
    }

    @PostMapping
    public ResponseEntity<Product> add(@RequestBody @Valid Product product, @RequestParam(name = "owner_id") Long ownerId) {
        return ResponseEntity.ok(service.add(product, ownerId));
    }
}
