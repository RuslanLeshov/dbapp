package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import ru.nsu.ccfit.database.leshov.domain.Customer;
import ru.nsu.ccfit.database.leshov.domain.Provider;
import ru.nsu.ccfit.database.leshov.services.ProviderService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/provider")
public class ProviderController {
    private final ProviderService service;

    public ProviderController(ProviderService service) {
        this.service = service;
    }

    @GetMapping
    public Page<Provider> getAll(@PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    public ResponseEntity<Provider> add(@RequestBody @Valid Provider provider) {
        return ResponseEntity.ok(service.add(provider));
    }

    @GetMapping("/filter")
    public Page<Provider> filter(@RequestParam(required = false) String filter,
                                 @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable) {
        if (filter == null) {
            return getAll(pageable);
        }

        return service.filterByProductName(filter, pageable);
    }

    @PutMapping("/{id}")
    public Provider updateProvider(@PathVariable("id") Provider savedProvider, @RequestBody @Valid Provider newProvider) {
        return service.update(savedProvider, newProvider);
    }

    @DeleteMapping("/{id}")
    public void deleteProvider(@PathVariable("id") Provider provider) {
        service.delete(provider);
    }
}
