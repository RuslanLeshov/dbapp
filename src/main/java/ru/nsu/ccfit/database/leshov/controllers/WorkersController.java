package ru.nsu.ccfit.database.leshov.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Worker;
import ru.nsu.ccfit.database.leshov.services.WorkerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkersController {
    private final WorkerService service;

    public WorkersController(WorkerService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<Worker>> getAllByOutlet(@RequestParam(name = "outlet_id") Outlet outlet) {
        return ResponseEntity.ok(service.getAllWorkersByOutlet(outlet));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Worker> get(@PathVariable(name = "id") Worker worker) {
        return ResponseEntity.ok(worker);
    }


    @GetMapping("/count")
    public ResponseEntity<Integer> countWorkers(@RequestParam(name = "outlet_id", required = false) Outlet outlet) {
        if (outlet != null) {
            return ResponseEntity.ok(service.getAllWorkersByOutlet(outlet).size());
        } else return ResponseEntity.ok(service.getAllWorkers().size());
    }

    @GetMapping("/output")
    public Double getOutput(@RequestParam("worker_id") Worker worker) {
        return service.getOutput(worker);
    }


    @PostMapping
    public ResponseEntity<Worker> create(@RequestBody @Valid Worker worker, @RequestParam Outlet outlet) {
        return ResponseEntity.ok(service.createWorker(worker, outlet));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Worker> update(@PathVariable(name = "id") Worker savedWorker,
                                         @RequestBody @Valid Worker worker) {
        BeanUtils.copyProperties(worker, savedWorker, "id", "outlet");

        return ResponseEntity.ok(service.updateWorker(savedWorker));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") Worker worker) {
        service.deleteWorker(worker);
        return ResponseEntity.ok().build();
    }
}
