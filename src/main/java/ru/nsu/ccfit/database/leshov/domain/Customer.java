package ru.nsu.ccfit.database.leshov.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
public class Customer extends DealParty {
    @NotBlank
    @Length(max = 60)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
