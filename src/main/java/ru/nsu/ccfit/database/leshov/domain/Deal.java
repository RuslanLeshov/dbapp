package ru.nsu.ccfit.database.leshov.domain;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "seller_id")
    private DealParty seller;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "buyer_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private DealParty buyer;

    @OneToMany
    private List<Product> products;

    Timestamp time;

    public Deal() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DealParty getSeller() {
        return seller;
    }

    public void setSeller(DealParty seller) {
        this.seller = seller;
    }

    public DealParty getBuyer() {
        return buyer;
    }

    public void setBuyer(DealParty buyer) {
        this.buyer = buyer;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
