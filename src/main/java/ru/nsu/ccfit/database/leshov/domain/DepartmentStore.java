package ru.nsu.ccfit.database.leshov.domain;

import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@Entity
public class DepartmentStore extends Outlet {
    @Positive
    private int hallsCount;

    @Positive
    private int floorsCount;

    public DepartmentStore() {
    }

    @Override
    public String getType() {
        return "Универмаг";
    }

    public int getHallsCount() {
        return hallsCount;
    }

    public void setHallsCount(int hallsCount) {
        this.hallsCount = hallsCount;
    }

    public int getFloorsCount() {
        return floorsCount;
    }

    public void setFloorsCount(int floorsCount) {
        this.floorsCount = floorsCount;
    }
}
