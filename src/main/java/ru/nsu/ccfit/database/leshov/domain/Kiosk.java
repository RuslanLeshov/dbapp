package ru.nsu.ccfit.database.leshov.domain;

import javax.persistence.Entity;

@Entity
public class Kiosk extends Outlet {
    @Override
    public String getType() {
        return "Киоск";
    }
}
