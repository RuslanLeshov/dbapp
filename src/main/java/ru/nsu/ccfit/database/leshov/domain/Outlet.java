package ru.nsu.ccfit.database.leshov.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Outlet extends DealParty {
    @OneToOne
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @OneToMany
    @JoinColumn(name = "outlet_id")
    private List<Worker> workers;

    @NotBlank
    @Length(max = 60)
    private String name;

    @Positive
    private double size;

    @Positive
    private double rentalFee;

    @Positive
    private double utilityBills;

    @Positive
    private int workplacesCount;

    public Outlet() { }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return null;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getRentalFee() {
        return rentalFee;
    }

    public void setRentalFee(double rentalFee) {
        this.rentalFee = rentalFee;
    }

    public double getUtilityBills() {
        return utilityBills;
    }

    public void setUtilityBills(double utilityBills) {
        this.utilityBills = utilityBills;
    }

    public int getWorkplacesCount() {
        return workplacesCount;
    }

    public void setWorkplacesCount(int workplacesCount) {
        this.workplacesCount = workplacesCount;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }
}
