package ru.nsu.ccfit.database.leshov.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

@Entity
public class Provider extends DealParty {
    @OneToOne
    @JoinColumn(name = "storage_id")
    private Storage storage;

    @NotBlank
    @Length(max = 60)
    private String name;

    @NotBlank
    @Length(max = 255)
    private String description;

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
