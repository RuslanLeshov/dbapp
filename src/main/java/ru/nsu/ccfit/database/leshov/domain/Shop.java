package ru.nsu.ccfit.database.leshov.domain;

import javax.persistence.Entity;
import javax.validation.constraints.Positive;

@Entity
public class Shop extends Outlet {
    @Positive
    private int hallsCount;

    public Shop() {
        super();
    }

    @Override
    public String getType() {
        return "Магазин";
    }

    public int getHallsCount() {
        return hallsCount;
    }

    public void setHallsCount(int hallsCount) {
        this.hallsCount = hallsCount;
    }
}
