package ru.nsu.ccfit.database.leshov.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Storage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Product> products;

    public Storage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Product getProductByName(String name) {
        final Product[] product = new Product[1];
        products.forEach(p -> {
            if (p.getName().equals(name)) {
                product[0] = p;
            }
        });

        return product[0];
    }
}
