package ru.nsu.ccfit.database.leshov.dtos;

import ru.nsu.ccfit.database.leshov.domain.DepartmentStore;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Shop;

public class OutletDto {
    private Long id;

    private String name;

    private StorageDto storage;

    private Double size;

    private Double rentalFee;

    private Double utilityBills;

    private Integer workplacesCount;

    private Integer hallsCount;

    private Integer floorsCount;

    private String type;

    public OutletDto(Outlet outlet) {
        id = outlet.getId();
        name = outlet.getName();
        storage = new StorageDto(outlet.getStorage());

        size = outlet.getSize();
        rentalFee = outlet.getRentalFee();
        utilityBills = outlet.getUtilityBills();
        workplacesCount = outlet.getWorkplacesCount();

        if (outlet instanceof Shop) {
            this.hallsCount = ((Shop) outlet).getHallsCount();
        }

        if (outlet instanceof DepartmentStore) {
            this.hallsCount = ((DepartmentStore) outlet).getHallsCount();
            this.floorsCount = ((DepartmentStore) outlet).getFloorsCount();
        }

        type = outlet.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StorageDto getStorage() {
        return storage;
    }

    public void setStorage(StorageDto storage) {
        this.storage = storage;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getRentalFee() {
        return rentalFee;
    }

    public void setRentalFee(Double rentalFee) {
        this.rentalFee = rentalFee;
    }

    public Double getUtilityBills() {
        return utilityBills;
    }

    public void setUtilityBills(Double utilityBills) {
        this.utilityBills = utilityBills;
    }

    public Integer getWorkplacesCount() {
        return workplacesCount;
    }

    public void setWorkplacesCount(Integer workplacesCount) {
        this.workplacesCount = workplacesCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getHallsCount() {
        return hallsCount;
    }

    public void setHallsCount(Integer hallsCount) {
        this.hallsCount = hallsCount;
    }

    public Integer getFloorsCount() {
        return floorsCount;
    }

    public void setFloorsCount(Integer floorsCount) {
        this.floorsCount = floorsCount;
    }
}
