package ru.nsu.ccfit.database.leshov.dtos;

import ru.nsu.ccfit.database.leshov.domain.Product;

public class ProductDto {
    private Long id;
    private String name;
    private int quantity;
    private double unitPrice;

    public ProductDto(Product product) {
        id = product.getId();
        name = product.getName();
        quantity = product.getQuantity();
        unitPrice = product.getUnitPrice();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
