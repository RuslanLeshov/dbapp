package ru.nsu.ccfit.database.leshov.dtos;

import ru.nsu.ccfit.database.leshov.domain.Storage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StorageDto {
    private Long id;
    private List<ProductDto> products;

    public StorageDto(Storage storage) {
        id = storage.getId();
        if (storage.getProducts() == null) {
            products = Collections.emptyList();
        } else {
            products = storage.getProducts()
                    .stream()
                    .map(ProductDto::new)
                    .collect(Collectors.toList());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }
}
