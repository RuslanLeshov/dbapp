package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.ccfit.database.leshov.domain.Customer;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("select c " +
            "from Customer c " +
            "join Deal d on c = d.buyer " +
            "join Product p on p member of d.products " +
            "and lower(p.name) like %:product% " +
            "group by c " +
            "order by sum(p.unitPrice * p.quantity) desc "
    )
    List<Customer> findAllByProductBought(@Param("product") String productName);

    @Query("select c " +
            "from Customer c " +
            "join Deal d on c = d.buyer " +
            "join Product p on p member of d.products " +
            "group by c " +
            "order by sum(p.unitPrice * p.quantity) desc ")
    List<Customer> findMostActive();
}
