package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.database.leshov.domain.DealParty;

public interface DealPartyRepository extends JpaRepository<DealParty, Long> {
}
