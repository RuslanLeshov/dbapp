package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import ru.nsu.ccfit.database.leshov.domain.Deal;
import ru.nsu.ccfit.database.leshov.domain.Outlet;

import java.sql.Timestamp;


public interface DealRepository extends PagingAndSortingRepository<Deal, Long> {
    Page<Deal> findAll();

    Page<Deal> findAllBySeller_Id(Long sellerId, Pageable pageable);

    Page<Deal> findAllByBuyer_Id(Long buyerId, Pageable pageable);

    Page<Deal> findAllByTimeBefore(Timestamp timestamp, Pageable pageable);

    Page<Deal> findAllByTimeAfter(Timestamp timestamp, Pageable pageable);

    Page<Deal> findAllByTimeBetween(Timestamp first, Timestamp second, Pageable pageable);

    @Query("select d from Deal d where d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Worker w on d.seller = w " +
            "join Outlet o on w member of o.workers and o = :outlet " +
            "where d.time < :time) " +
            "or d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Outlet o on o = d.buyer and o = :outlet " +
            "where d.time < :time)")
    Page<Deal> findAllByOutletAndTimeBefore(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d where d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Worker w on d.seller = w " +
            "join Outlet o on w member of o.workers and o = :outlet " +
            "where d.time > :time) " +
            "or d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Outlet o on o = d.buyer and o = :outlet " +
            "where d.time > :time) ")
    Page<Deal> findAllByOutletAndTimeAfter(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d where d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Worker w on d.seller = w " +
            "join Outlet o on w member of o.workers and o = :outlet " +
            "where d.time between :first and :second) " +
            "or d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Outlet o on o = d.buyer and o = :outlet " +
            "where d.time between :first and :second) "
    )
    Page<Deal> findAllByOutletAndTimeBetween(@Param("outlet") Outlet outlet, @Param("first") Timestamp first, @Param("second") Timestamp second, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers and o = :outlet where d.time < :time")
    Page<Deal> findAllIncomeDealsByOutletAndTimeBefore(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers where d.time < :time")
    Page<Deal> findAllIncomeDealsByTimeBefore(@Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers and o = :outlet where d.time > :time")
    Page<Deal> findAllIncomeDealsByOutletAndTimeAfter(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers where d.time > :time")
    Page<Deal> findAllIncomeDealsByTimeAfter(@Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers and o = :outlet where d.time between :first and :second")
    Page<Deal> findAllIncomeDealsByOutletAndTimeBetween(@Param("outlet") Outlet outlet, @Param("first") Timestamp first, @Param("second") Timestamp second, Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers where d.time between :first and :second")
    Page<Deal> findAllIncomeDealsByTimeBetween(@Param("first") Timestamp first, @Param("second") Timestamp second, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer and o = :outlet where d.time < :time")
    Page<Deal> findAllExpenseDealsByOutletAndTimeBefore(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer where d.time < :time")
    Page<Deal> findAllExpenseDealsByTimeBefore(@Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer and o = :outlet where d.time > :time")
    Page<Deal> findAllExpenseDealsByOutletAndTimeAfter(@Param("outlet") Outlet outlet, @Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer where d.time > :time")
    Page<Deal> findAllExpenseDealsByTimeAfter(@Param("time") Timestamp time, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer and o = :outlet where d.time between :first and :second")
    Page<Deal> findAllExpenseDealsByOutletAndTimeBetween(@Param("outlet") Outlet outlet, @Param("first") Timestamp first, @Param("second") Timestamp second, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o = d.buyer where d.time between :first and :second")
    Page<Deal> findAllExpenseDealsByTimeBetween(@Param("first") Timestamp first, @Param("second") Timestamp second, Pageable pageable);


    @Query("select d from Deal d where d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Worker w ON d.seller = w " +
            "join Outlet o ON w member of o.workers and o.id = :outlet_id) " +
            " or d.id in " +
            "(select d.id " +
            "from Deal d " +
            "join Outlet o ON o = d.buyer and o.id = :outlet_id " +
            "join Product p ON p member of d.products)")
    Page<Deal> findAllByOutletId(@Param("outlet_id") Long outletId, Pageable pageable);


    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers")
    Page<Deal> findAllIncomeDeals(Pageable pageable);

    @Query("select d from Deal d join Worker w on w = d.seller join Outlet o on w member of o.workers and o = :outlet")
    Page<Deal> findAllIncomeDealsByOutlet(@Param("outlet") Outlet outlet, Pageable pageable);

    @Query("select d from Deal d join Outlet o on o.id = d.buyer.id")
    Page<Deal> findAllExpenseDeals(Pageable pageable);

    @Query("select d from Deal d join Outlet o on o.id = d.buyer.id and o = :outlet")
    Page<Deal> findAllExpenseDealsByOutlet(@Param("outlet") Outlet outlet, Pageable pageable);

    @Query(nativeQuery = true,
            value = "select SUM(p.quantity * p.unit_price) " +
                    "from product p join deal_products dp ON dp.products_id = p.id " +
                    "join deal d on d.seller_id = :seller_id AND dp.deal_id = d.id")
    Double getIncomeBySellerId(@Param("seller_id") Long sellerId);

    @Query(nativeQuery = true,
            value = "select SUM(p.quantity * p.unit_price) " +
                    "from product p join deal_products dp on p.id = dp.products_id " +
                    "join deal d on d.id = dp.deal_id " +
                    "join worker w on d.seller_id = w.id " +
                    "join outlet o on w.outlet_id = o.id and o.id = :outlet_id")
    Double getIncomeByOutletId(@Param("outlet_id") Long outletId);

    @Query(nativeQuery = true,
            value = "select SUM(p.quantity * p.unit_price) " +
                    "from product p join deal_products dp ON dp.products_id = p.id " +
                    "join deal d on d.buyer_id = :buyer_id AND dp.deal_id = d.id")
    Double getExpenseByBuyerId(@Param("buyer_id") Long buyerId);

    @Query(nativeQuery = true,
            value = "select SUM(p.quantity * p.unit_price) " +
                    "from product p join deal_products dp ON dp.products_id = p.id " +
                    "join deal d on dp.deal_id = d.id " +
                    "join worker w on w.id = d.seller_id " +
                    "join outlet o on o.id = w.outlet_id "
    )
    Double getTotalIncome();

    @Query(nativeQuery = true,
            value = "select SUM(p.quantity * p.unit_price) " +
                    "from product p join deal_products dp ON dp.products_id = p.id " +
                    "join deal d on dp.deal_id = d.id " +
                    "join outlet o on o.id = d.buyer_id"
    )
    Double getTotalExpense();
}
