package ru.nsu.ccfit.database.leshov.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.database.leshov.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> { }
