package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import ru.nsu.ccfit.database.leshov.domain.Provider;

public interface ProviderRepository extends PagingAndSortingRepository<Provider, Long> {
    Page<Provider> findAll(Pageable pageable);

    Page<Provider> findAllByIdIn(Iterable<Long> id, Pageable pageable);

    @Query("select distinct pr " +
            "from Provider pr " +
            "join Storage s on pr.storage = s " +
            "join Product p ON p member of s.products " +
            "where lower(p.name) like %:product%")
    Page<Provider> findAllByProductSold(@Param("product") String productName, Pageable pageable);
}
