package ru.nsu.ccfit.database.leshov.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.database.leshov.domain.Customer;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Provider;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class QueryRepository extends JdbcDaoSupport {


    public QueryRepository(DataSource dataSource) {

        this.setDataSource(dataSource);
    }


    /**
     * Получить перечень поставщиков, поставляющих указанный вид товара
     */

    public List<Long> getAllProvidersByProductSold(String productName) {

        String sql = String.join(" ",
                "SELECT DISTINCT pr.id",
                "FROM provider pr",
                "INNER JOIN storage ON pr.storage_id = storage.id",
                "INNER JOIN storage_products ON storage_products.storage_id = storage.id",
                "INNER JOIN product ON storage_products.products_id = product.id",
                "AND product.name ILIKE '%" + productName + "%'");



        JdbcTemplate template = this.getJdbcTemplate();

        assert (template != null);
        return template.queryForList(sql, Long.class);
    }

    /**
     * Получить перечень покупателей, купивших указанный вид товара за некоторый период
     */
    public List<Customer> getAllCustomersByProductBought(Product product, Timestamp from, Timestamp to) {
        String sql = String.join(" ",
                "SELECT c.*",
                "FROM trading_org.customer c",
                "INNER JOIN trading_org.deal d ON d.buyer_id = c.dp_id"
        );
        if (from != null && to != null) {
            sql = String.join(" ", sql, "AND d.date between", from.toString(), "and", to.toString());
        }

        sql = String.join(" ", sql,
                "INNER JOIN trading_org.deal_product dp ON dp.deal_id = d.deal_id",
                "INNER JOIN trading_org.product pr ON pr.product_id = dp.product_id AND pr.name = 'Bread'"
        );

        JdbcTemplate template = this.getJdbcTemplate();
        assert (template != null);
        return template.queryForList(sql, Customer.class);
    }



}
