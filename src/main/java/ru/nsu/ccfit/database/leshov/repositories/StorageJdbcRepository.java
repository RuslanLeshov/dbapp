package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Storage;

import javax.sql.DataSource;

@Repository
public class StorageJdbcRepository extends JdbcDaoSupport {
    public StorageJdbcRepository(DataSource dataSource) {
        this.setDataSource(dataSource);
    }


    public Long getStorageIdByOwnerId(Long id) {
        String sql = "SELECT st.id " +
                "FROM storage st " +
                "INNER JOIN provider pr ON pr.storage_id=st.id AND pr.id = " + id + " " +
                "UNION " +
                "SELECT st.* " +
                "FROM storage st " +
                "INNER JOIN outlet o ON o.storage_id=st.id AND o.id = " + id;
        assert this.getJdbcTemplate() != null;

        return this.getJdbcTemplate().queryForObject(sql, Long.class);
    }

    public void removeProductInStorage(Long storageId, Long productId) {
        String sql = String.join("", "DELETE FROM storage_products sp WHERE sp.products_id = ", "" + productId,
                " AND sp.storage_id = ", "" + storageId);
        assert this.getJdbcTemplate() != null;

        this.getJdbcTemplate().execute(sql);
    }
}
