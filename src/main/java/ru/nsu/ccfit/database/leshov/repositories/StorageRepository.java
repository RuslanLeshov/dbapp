package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Storage;

import java.util.List;

public interface StorageRepository extends JpaRepository<Storage, Long> {

    @Query("select st from Storage st join Provider pr on pr.id = :owner_id and pr.storage = st")
    Storage getStorageByProviderId(Long owner_id);

    @Query("select st from Storage st join Outlet o on o.id = :owner_id and o.storage = st")
    Storage getStorageByOutletId(Long owner_id);

    default Storage getStorageByOwnerId(Long owner_id) {
        Storage storage = getStorageByOutletId(owner_id);
        if (storage == null) return getStorageByProviderId(owner_id);
        return storage;
    }
}
