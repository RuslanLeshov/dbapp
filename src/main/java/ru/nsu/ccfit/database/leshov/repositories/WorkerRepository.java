package ru.nsu.ccfit.database.leshov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Worker;

import java.util.List;

public interface WorkerRepository extends JpaRepository<Worker, Long> {
    @Query("select sum(p.quantity * p.unitPrice) from Product p join Deal d on p member of d.products join Worker w on w = :worker and w = d.seller")
    Double getOutput(@Param("worker") Worker worker);
}
