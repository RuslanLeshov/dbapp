package ru.nsu.ccfit.database.leshov.repositories.outlets;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.database.leshov.domain.DepartmentStore;

public interface DepartmentStoreRepository extends JpaRepository<DepartmentStore, Long> {
}
