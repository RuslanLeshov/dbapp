package ru.nsu.ccfit.database.leshov.repositories.outlets;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.database.leshov.domain.Kiosk;

public interface KioskRepository extends JpaRepository<Kiosk, Long> {
}
