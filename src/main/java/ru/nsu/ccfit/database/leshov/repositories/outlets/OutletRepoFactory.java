package ru.nsu.ccfit.database.leshov.repositories.outlets;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.DepartmentStore;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Shop;

import java.util.HashMap;

@Service
public class OutletRepoFactory {
    private final HashMap<Class<? extends Outlet>,
            JpaRepository<? extends Outlet, Long>> map = new HashMap<>();

    public OutletRepoFactory(OutletRepository outletRepository,
                             ShopRepository shopRepository,
                             DepartmentStoreRepository departmentStoreRepository) {
        map.put(Outlet.class, outletRepository);
        map.put(Shop.class, shopRepository);
        map.put(DepartmentStore.class, departmentStoreRepository);
    }

    @SuppressWarnings("unchecked")
    public <T extends Outlet> JpaRepository<T, Long> get(T outlet) {
        return (JpaRepository<T, Long>) map.get(outlet.getClass());
    }
}
