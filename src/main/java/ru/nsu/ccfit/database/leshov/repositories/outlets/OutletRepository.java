package ru.nsu.ccfit.database.leshov.repositories.outlets;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.ccfit.database.leshov.domain.Outlet;

public interface OutletRepository extends JpaRepository<Outlet, Long> {
    @Query("select sum (p.quantity * p.unitPrice) / o.workers.size from Product p join Deal d on p member of d.products join Worker w on w = d.seller join Outlet o on w member of o.workers and o = :outlet group by o")
    Double getOutput(@Param("outlet") Outlet outlet);

    @Query("select sum(p.quantity * p.unitPrice) / (sum(w.salary) + o.rentalFee + o.utilityBills) from Product p join Deal d on p member of d.products join Worker w on w = d.seller join Outlet o on o=:outlet and w member of o.workers group by o")
    Double getEfficiency(@Param("outlet") Outlet outlet);

    @Query("select sum(w.salary) from Worker w join Outlet o on w member of o.workers where o = :outlet")
    Double getTotalWorkersSalary(@Param("outlet") Outlet outlet);
}
