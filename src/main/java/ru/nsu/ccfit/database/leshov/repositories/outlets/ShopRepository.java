package ru.nsu.ccfit.database.leshov.repositories.outlets;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.database.leshov.domain.Shop;

public interface ShopRepository extends JpaRepository<Shop, Long> {
}
