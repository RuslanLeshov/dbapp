package ru.nsu.ccfit.database.leshov.services;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.Customer;
import ru.nsu.ccfit.database.leshov.repositories.CustomerRepository;

import java.util.List;

@Service
public class CustomerService {
    private final CustomerRepository repository;

    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<Customer> getAll() {
        return repository.findAll();
    }


    public Customer add(Customer customer) {
        return repository.save(customer);
    }

    public List<Customer> filterByProductName(String filter) {
        return repository.findAllByProductBought(filter.toLowerCase());
    }

    public List<Customer> findMostActive() {
        return repository.findMostActive();
    }

    public Customer update(Customer savedCustomer, Customer newCustomer) {
        BeanUtils.copyProperties(newCustomer, savedCustomer, "id");
        return repository.save(savedCustomer);
    }

    public void delete(Customer customer) {
        repository.delete(customer);
    }
}
