package ru.nsu.ccfit.database.leshov.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.*;
import ru.nsu.ccfit.database.leshov.repositories.DealRepository;
import ru.nsu.ccfit.database.leshov.repositories.ProductRepository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DealService {
    private final DealRepository repository;
    private final ProductRepository productRepository;
    private final StorageManager storageManager;

    public DealService(DealRepository repository, ProductRepository productRepository, StorageManager storageManager) {
        this.repository = repository;
        this.productRepository = productRepository;
        this.storageManager = storageManager;
    }


    @Transactional
    public Deal add(List<Product> products, DealParty seller, DealParty buyer, Outlet outlet) {
        Deal deal = new Deal();
        deal.setBuyer(buyer);
        deal.setSeller(seller);
        deal.setProducts(products);

        Storage sellerStorage = storageManager.getStorageByOwnerId(seller.getId());
        if (sellerStorage == null) {
            sellerStorage = outlet.getStorage();
        }
        Storage buyerStorage = storageManager.getStorageByOwnerId(buyer.getId());


        for (Product dealProduct : products) {
            Product soldProduct = sellerStorage.getProductByName(dealProduct.getName());
            if (dealProduct.getQuantity() > soldProduct.getQuantity()) {
                throw new RuntimeException("Попытка продать больше чем есть: склад " + sellerStorage.getId() + ", товар " + soldProduct.getId());
            } else if (dealProduct.getQuantity() == soldProduct.getQuantity()) {
                storageManager.removeProduct(sellerStorage, soldProduct);
            } else {
                soldProduct.setQuantity(soldProduct.getQuantity() - dealProduct.getQuantity());
                productRepository.save(soldProduct);
            }

            if (buyerStorage != null) {
                Product boughtProduct = buyerStorage.getProductByName(dealProduct.getName());
                if (boughtProduct == null) {
                    boughtProduct = new Product();
                    boughtProduct.setName(dealProduct.getName());
                    boughtProduct.setQuantity(dealProduct.getQuantity());
                    boughtProduct.setUnitPrice(dealProduct.getUnitPrice());

                    Product savedProduct = productRepository.save(boughtProduct);
                    storageManager.addProduct(buyerStorage, savedProduct);
                } else {
                    boughtProduct.setQuantity(boughtProduct.getQuantity() + dealProduct.getQuantity());
                    productRepository.save(boughtProduct);
                }
            }
        }
        deal.setTime(Timestamp.valueOf(LocalDateTime.now()));

        productRepository.saveAll(products);

        return repository.save(deal);
    }

    public Double getIncome(Outlet outlet) {
        Double income = repository.getIncomeByOutletId(outlet.getId());
        if (income == null) return 0.0;
        return income;
    }

    public Double getTotalIncome() {
        Double income = repository.getTotalIncome();
        if (income == null) return 0.0;
        return income;
    }

    public Double getExpense(Outlet outlet) {
        Double expense = repository.getExpenseByBuyerId(outlet.getId());
        if (expense == null) return 0.0;
        return expense;
    }

    public Double getTotalExpense() {
        Double expense = repository.getTotalExpense();
        if (expense == null) return 0.0;
        return expense;
    }

    public Page<Deal> getAll(Outlet outletFilter, String dealTypeFilter, String dateFrom, String dateTo, Pageable pageable) {
        if (outletFilter == null) {
            return getAllDealsByTypeAndDates(dealTypeFilter, dateFrom, dateTo, pageable);
        }

        if (dealTypeFilter == null) {
            return getDealsByDatesAndOutlet(outletFilter, dateFrom, dateTo, pageable);
        }
        if (dealTypeFilter.equals("income")) {
            return findAllIncomeDealsByDatesAndOutlet(outletFilter, dateFrom, dateTo, pageable);
        }
        if (dealTypeFilter.equals("expense")) {
            return findAllExpenseDealsByDatesAndOutlet(outletFilter, dateFrom, dateTo, pageable);
        }
        return repository.findAll(pageable);
    }

    private Page<Deal> findAllExpenseDealsByDatesAndOutlet(Outlet outletFilter, String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            if (dateTo == null) {
                return repository.findAllExpenseDealsByOutlet(outletFilter, pageable);
            }
            return repository.findAllExpenseDealsByOutletAndTimeBefore(outletFilter, Timestamp.valueOf(dateTo), pageable);
        }
        if (dateTo == null) {
            return repository.findAllExpenseDealsByOutletAndTimeAfter(outletFilter, Timestamp.valueOf(dateFrom), pageable);
        }

        return repository.findAllExpenseDealsByOutletAndTimeBetween(outletFilter,
                Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> findAllIncomeDealsByDatesAndOutlet(Outlet outletFilter, String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            if (dateTo == null) {
                return repository.findAllIncomeDealsByOutlet(outletFilter, pageable);
            }
            return repository.findAllIncomeDealsByOutletAndTimeBefore(outletFilter, Timestamp.valueOf(dateTo), pageable);
        }
        if (dateTo == null) {
            return repository.findAllIncomeDealsByOutletAndTimeAfter(outletFilter, Timestamp.valueOf(dateFrom), pageable);
        }

        return repository.findAllIncomeDealsByOutletAndTimeBetween(outletFilter, Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> getDealsByDatesAndOutlet(Outlet outletFilter, String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            if (dateTo == null) {
                return repository.findAllByOutletId(outletFilter.getId(), pageable);
            }
            return repository.findAllByOutletAndTimeBefore(outletFilter, Timestamp.valueOf(dateTo), pageable);
        }
        if (dateTo == null) {
            return repository.findAllByOutletAndTimeAfter(outletFilter, Timestamp.valueOf(dateFrom), pageable);
        }

        return repository.findAllByOutletAndTimeBetween(outletFilter, Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> getAllDealsByTypeAndDates(String dealTypeFilter, String dateFrom, String dateTo, Pageable pageable) {
        if (dealTypeFilter == null) {
            return getAllDealsByDates(dateFrom, dateTo, pageable);
        }
        if (dealTypeFilter.equals("income")) {
            return findAllIncomeDealsByDates(dateFrom, dateTo, pageable);
        }
        if (dealTypeFilter.equals("expense")) {
            return findAllExpenseDealsByDates(dateFrom, dateTo, pageable);
        }
        return repository.findAll(pageable);
    }

    private Page<Deal> findAllExpenseDealsByDates(String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            if (dateTo == null) {
                return repository.findAllExpenseDeals(pageable);
            }
            return repository.findAllExpenseDealsByTimeBefore(Timestamp.valueOf(dateTo), pageable);
        }
        if (dateTo == null) {
            return repository.findAllExpenseDealsByTimeAfter(Timestamp.valueOf(dateFrom), pageable);
        }

        return repository.findAllExpenseDealsByTimeBetween(Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> findAllIncomeDealsByDates(String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            if (dateTo == null) {
                return repository.findAllIncomeDeals(pageable);
            }
            return repository.findAllIncomeDealsByTimeBefore(Timestamp.valueOf(dateTo), pageable);
        }
        if (dateTo == null) {
            return repository.findAllIncomeDealsByTimeAfter(Timestamp.valueOf(dateFrom), pageable);
        }

        return repository.findAllIncomeDealsByTimeBetween(Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> getAllDealsByDates(String dateFrom, String dateTo, Pageable pageable) {
        if (dateFrom == null) {
            return getAllDealsByDateTo(dateTo, pageable);
        }
        if (dateTo == null) {
            return getAllDealsByDateFrom(dateFrom, pageable);
        }

        return repository.findAllByTimeBetween(Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo), pageable);
    }

    private Page<Deal> getAllDealsByDateFrom(String dateFrom, Pageable pageable) {
        if (dateFrom == null) return repository.findAll(pageable);

        return repository.findAllByTimeAfter(Timestamp.valueOf(dateFrom), pageable);
    }

    private Page<Deal> getAllDealsByDateTo(String dateTo, Pageable pageable) {
        if (dateTo == null) return repository.findAll(pageable);

        return repository.findAllByTimeBefore(Timestamp.valueOf(dateTo), pageable);
    }

    public Page<Deal> getAllBySellerId(Long sellerId, Pageable pageable) {
        return repository.findAllBySeller_Id(sellerId, pageable);
    }

    public Page<Deal> getAllByBuyerId(Long buyerId, PageRequest pageable) {
        return repository.findAllByBuyer_Id(buyerId, pageable);
    }
}
