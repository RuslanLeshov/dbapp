package ru.nsu.ccfit.database.leshov.services;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Storage;
import ru.nsu.ccfit.database.leshov.repositories.outlets.OutletRepoFactory;
import ru.nsu.ccfit.database.leshov.repositories.outlets.OutletRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class OutletService {
    private final StorageManager storageManager;
    private final OutletRepoFactory repoFactory;
    private final OutletRepository repository;

    public OutletService(StorageManager storageManager, OutletRepoFactory repoFactory, OutletRepository repository) {
        this.storageManager = storageManager;
        this.repoFactory = repoFactory;
        this.repository = repository;
    }

    @Transactional
    public Outlet createOutlet(Outlet outlet) {
        Storage storage = storageManager.createStorage();
        outlet.setStorage(storage);
        JpaRepository<? extends Outlet, Long> outletLongJpaRepository = repoFactory.get(outlet.getClass().cast(outlet));
        repository.save(outlet.getClass().cast(outlet));
        return outlet;
    }


    public List<Outlet> getAllOutlets() {
        return repository.findAll();
    }

    public Outlet getOutlet(Long id) {
        Optional<Outlet> outlet = repository.findById(id);
        if (outlet.isPresent()) {
            return outlet.get();
        } else {
            throw new RuntimeException("Not found");
        }
    }

    public Outlet updateOutlet(Outlet outlet, Outlet storedOutlet) {
        BeanUtils.copyProperties(outlet, storedOutlet, "id", "storage");
        return repository.save(storedOutlet);
    }

    public void deleteOutlet(Outlet outlet) {
        repository.delete(outlet);
    }

    public double calculateOutput(Outlet outlet) {
        Double output = repository.getOutput(outlet);
        if (output == null) return 0.0;
        return output;
    }

    public Double calculateEfficiency(Outlet outlet) {
        Double efficiency = repository.getEfficiency(outlet);
        if (efficiency == null) return 0.0;
        return efficiency;
    }

    public Double getTotalWorkersSalary(Outlet outlet) {
        return repository.getTotalWorkersSalary(outlet);
    }
}
