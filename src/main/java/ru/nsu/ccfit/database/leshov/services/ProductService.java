package ru.nsu.ccfit.database.leshov.services;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.DealParty;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Storage;
import ru.nsu.ccfit.database.leshov.repositories.ProductRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductService {
    private final ProductRepository repository;
    private final StorageManager storageManager;

    public ProductService(ProductRepository repository, StorageManager storageManager) {
        this.repository = repository;
        this.storageManager = storageManager;
    }


    public List<Product> getAll(Long ownerId) {
        Storage storage = storageManager.getStorageByOwnerId(ownerId);

        return storage.getProducts();
    }

    @Transactional
    public Product add(Product product, Long ownerId) {
        Storage storage = storageManager.getStorageByOwnerId(ownerId);

        Product storedProduct = storage.getProductByName(product.getName());
        if (storedProduct != null) {
            storedProduct.setUnitPrice(product.getUnitPrice());
            storedProduct.setQuantity(storedProduct.getQuantity() + product.getQuantity());

            storedProduct = repository.save(product);
            storageManager.update(storage);
            return storedProduct;
        }

        storedProduct = repository.save(product);

        storage.getProducts().add(storedProduct);
        storageManager.update(storage);

        return storedProduct;
    }
}
