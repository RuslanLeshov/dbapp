package ru.nsu.ccfit.database.leshov.services;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.Customer;
import ru.nsu.ccfit.database.leshov.domain.Provider;
import ru.nsu.ccfit.database.leshov.domain.Storage;
import ru.nsu.ccfit.database.leshov.repositories.ProviderRepository;
import ru.nsu.ccfit.database.leshov.repositories.QueryRepository;

import java.util.List;

@Service
public class ProviderService {
    private final ProviderRepository repository;
    private final StorageManager storageManager;


    public ProviderService(ProviderRepository repository, StorageManager storageManager) {
        this.repository = repository;
        this.storageManager = storageManager;
    }

    public Page<Provider> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Provider add(Provider provider) {
        Storage storage = storageManager.createStorage();
        provider.setStorage(storage);

        return repository.save(provider);
    }

    public Page<Provider> filterByProductName(String productName, Pageable pageable) {
        return repository.findAllByProductSold(productName.toLowerCase(), pageable);
    }

    public Provider update(Provider savedProvider, Provider newProvider) {
        BeanUtils.copyProperties(newProvider, savedProvider, "id");
        return repository.save(savedProvider);
    }

    public void delete(Provider provider) {
        repository.delete(provider);
    }
}