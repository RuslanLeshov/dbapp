package ru.nsu.ccfit.database.leshov.services;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.Product;
import ru.nsu.ccfit.database.leshov.domain.Storage;
import ru.nsu.ccfit.database.leshov.repositories.StorageJdbcRepository;
import ru.nsu.ccfit.database.leshov.repositories.StorageRepository;

import java.util.List;

@Service
public class StorageManager {
    private final StorageRepository storageRepository;
    private final StorageJdbcRepository jdbcRepository;

    public StorageManager(StorageRepository storageRepository, StorageJdbcRepository jdbcRepository) {
        this.storageRepository = storageRepository;
        this.jdbcRepository = jdbcRepository;
    }


    Storage createStorage() {
        Storage storage = new Storage();
        storageRepository.save(storage);
        return storage;
    }

    public List<Storage> getAllStorage() {
        return storageRepository.findAll();
    }

    public Storage getStorageByOwnerId(Long id) {
        return storageRepository.getStorageByOwnerId(id);
    }

    public void update(Storage storage) {
        storageRepository.save(storage);
    }

    public void removeProduct(Storage storage, Product product) {
        jdbcRepository.removeProductInStorage(storage.getId(), product.getId());
    }

    public void addProduct(Storage storage, Product product) {
        storage.getProducts().add(product);
        storageRepository.save(storage);
    }
}
