package ru.nsu.ccfit.database.leshov.services;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.database.leshov.domain.Outlet;
import ru.nsu.ccfit.database.leshov.domain.Worker;
import ru.nsu.ccfit.database.leshov.repositories.WorkerRepository;
import ru.nsu.ccfit.database.leshov.repositories.outlets.OutletRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {
    private final WorkerRepository repository;
    private final OutletRepository outletRepository;

    public WorkerService(WorkerRepository repository, OutletRepository outletRepository) {
        this.repository = repository;
        this.outletRepository = outletRepository;
    }

    @Transactional
    public Worker createWorker(Worker worker, Outlet outlet) {
        Worker savedWorker = repository.saveAndFlush(worker);
        outlet.getWorkers().add(savedWorker);
        outletRepository.save(outlet);
        return savedWorker;
    }

    public List<Worker> getAllWorkers() {
        return repository.findAll();
    }

    public Worker getWorker(Long id) {
        Optional<Worker> worker = repository.findById(id);
        if (worker.isPresent()) {
            return worker.get();
        } else {
            throw new RuntimeException("Not found");
        }
    }

    public Worker updateWorker(Worker worker) {
        return repository.save(worker);
    }

    public void deleteWorker(Worker worker) {
        repository.delete(worker);
    }

    public List<Worker> getAllWorkersByOutlet(Outlet outlet) {
        return outlet.getWorkers();
    }

    public Double getOutput(Worker worker) {
        Double output = repository.getOutput(worker);
        if (output == null) return 0.0;
        return output;
    }
}