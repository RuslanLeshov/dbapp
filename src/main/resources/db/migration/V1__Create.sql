create sequence hibernate_sequence start 1 increment 1;
create table customer
(
    id   int8 not null,
    name varchar(60),
    primary key (id)
);
create table deal
(
    id        int8 not null,
    time      timestamp,
    buyer_id  int8,
    seller_id int8,
    primary key (id)
);
create table deal_products
(
    deal_id     int8 not null,
    products_id int8 not null
);
create table deal_party
(
    id int8 not null,
    primary key (id)
);
create table department_store
(
    id int8 not null,
    primary key (id)
);
create table kiosk
(
    id int8 not null,
    primary key (id)
);
create table outlet
(
    id               int8   not null,
    name             varchar(60),
    rental_fee       float8 not null,
    size             float8 not null,
    utility_bills    float8 not null,
    workplaces_count int4   not null,
    storage_id       int8,
    primary key (id)
);
create table product
(
    id         int8   not null,
    name       varchar(60),
    quantity   int4   not null,
    unit_price float8 not null,
    primary key (id)
);
create table provider
(
    id          int8 not null,
    description varchar(255),
    name        varchar(60),
    storage_id  int8,
    primary key (id)
);
create table shop
(
    id          int8 not null,
    halls_count int4 not null,
    primary key (id)
);
create table storage
(
    id int8 not null,
    primary key (id)
);
create table storage_products
(
    storage_id  int8 not null,
    products_id int8 not null
);
create table worker
(
    id        int8   not null,
    name      varchar(60),
    salary    float8 not null,
    outlet_id int8,
    primary key (id)
);

alter table if exists deal_products
    add constraint deal_product_unique_constraint unique (products_id);
alter table if exists storage_products
    add constraint storage_product_unique_constraint unique (products_id);

alter table if exists customer
    add constraint fk_customer_deal_party_id foreign key (id) references deal_party;
alter table if exists deal
    add constraint fk_deal_deal_party_buyer_id foreign key (buyer_id) references deal_party;
alter table if exists deal
    add constraint fk_deal_deal_party_seller_id foreign key (seller_id) references deal_party;
alter table if exists deal_products
    add constraint fk_deal_products_product_products_id foreign key (products_id) references product;
alter table if exists deal_products
    add constraint fk_deal_products_deal_deal_id foreign key (deal_id) references deal;
alter table if exists department_store
    add constraint fk_department_store_outlet_id foreign key (id) references outlet;
alter table if exists kiosk
    add constraint fk_kiosk_outlet_id foreign key (id) references outlet;
alter table if exists outlet
    add constraint fk_outlet_storage_storage_id foreign key (storage_id) references storage;
alter table if exists outlet
    add constraint fk_outlet_deal_party_id foreign key (id) references deal_party;
alter table if exists provider
    add constraint fk_provider_storage_storage_id foreign key (storage_id) references storage;
alter table if exists provider
    add constraint fk_provider_deal_party_id foreign key (id) references deal_party;
alter table if exists shop
    add constraint fk_shop_outlet_id foreign key (id) references outlet;
alter table if exists storage_products
    add constraint fk_storage_products_product_products_id foreign key (products_id) references product;
alter table if exists storage_products
    add constraint fk_storage_products_storage_storage_id foreign key (storage_id) references storage;
alter table if exists worker
    add constraint fk_worker_outlet_outlet_id foreign key (outlet_id) references outlet;
alter table if exists worker
    add constraint fk_worker_deal_party_id foreign key (id) references deal_party;