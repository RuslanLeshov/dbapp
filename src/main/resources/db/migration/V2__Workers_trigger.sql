CREATE OR REPLACE FUNCTION count_workers(oid outlet.id%TYPE)
    RETURNS int8 AS
$BODY$
BEGIN
    return (SELECT COUNT(w) FROM worker w WHERE oid = w.outlet_id);
END;
$BODY$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_workers_limit(oid outlet.id%TYPE)
    RETURNS int8 AS
$BODY$
BEGIN
    return (SELECT o.workplaces_count FROM outlet o WHERE oid = o.id);
END;
$BODY$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_workers_limit()
    RETURNS trigger AS
$BODY$
BEGIN
    IF count_workers(NEW.outlet_id) >= get_workers_limit(NEW.outlet_id) THEN
        RAISE EXCEPTION 'Превышен лимит сотрудников торговой точки %', NEW.outlet_id;
    end if;
    RETURN NEW;
END
$BODY$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_workers_limit_on_update()
    RETURNS trigger AS
$BODY$
BEGIN
    IF OLD.outlet_id IS NULL THEN
        IF count_workers(NEW.outlet_id) >= get_workers_limit(NEW.outlet_id) THEN
            RAISE EXCEPTION 'Превышен лимит сотрудников торговой точки %', NEW.outlet_id;
        end if;
    end if;
    RETURN NEW;
END
$BODY$
    LANGUAGE plpgsql;

CREATE TRIGGER check_workers_limit_trigger
    BEFORE INSERT
    ON worker
    FOR EACH ROW
EXECUTE PROCEDURE check_workers_limit();



CREATE TRIGGER check_workers_limit_on_update
    BEFORE UPDATE
    ON worker
    FOR EACH ROW
EXECUTE PROCEDURE check_workers_limit_on_update();