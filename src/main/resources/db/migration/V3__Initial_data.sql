-- Торговые точки
insert into deal_party (id) values (1);
insert into storage (id) values (1);
insert into outlet (id, storage_id, name, rental_fee, size, utility_bills, workplaces_count)
values (1, 1, 'Магазин раз', 10, 11, 12, 3);

insert into deal_party (id) values (2);
insert into storage (id) values (2);
insert into outlet (id, storage_id, name, rental_fee, size, utility_bills, workplaces_count)
values (2, 2, 'Магазин два', 10, 11, 12, 3);

insert into deal_party (id) values (3);
insert into storage (id) values (3);
insert into outlet (id, storage_id, name, rental_fee, size, utility_bills, workplaces_count)
values (3, 3, 'Магазин три', 10, 11, 12, 3);

insert into deal_party (id) values (4);
insert into storage (id) values (4);
insert into outlet (id, storage_id, name, rental_fee, size, utility_bills, workplaces_count)
values (4, 4, 'Магазин четыре', 10, 11, 12, 3);

insert into deal_party (id) values (5);
insert into storage (id) values (5);
insert into outlet (id, storage_id, name, rental_fee, size, utility_bills, workplaces_count)
values (5, 5, 'Магазин пять', 10, 11, 12, 3);


-- Продавцы
insert into deal_party (id) values (6);
insert into worker(id, outlet_id, name, salary)
values (6, 1, 'Виктор Франкенштейн', 10000);

insert into deal_party (id) values (7);
insert into worker(id, outlet_id, name, salary)
values (7, 2, 'Пабло Пикассо', 11000);

insert into deal_party (id) values (8);
insert into worker(id, outlet_id, name, salary)
values (8, 3, 'Джеймс Бонд', 9000);

insert into deal_party (id) values (9);
insert into worker(id, outlet_id, name, salary)
values (9, 4, 'Гена Крокодил', 12000);

insert into deal_party (id) values (10);
insert into worker(id, outlet_id, name, salary)
values (10, 5, 'Игорь Рюрикович', 14000.38);

insert into deal_party (id) values (11);
insert into worker(id, outlet_id, name, salary)
values (11, 1, 'Агент 47', 8000.99);

insert into deal_party (id) values (12);
insert into worker(id, outlet_id, name, salary)
values (12, 1, 'Медведь Копатыч', 16000);

insert into deal_party (id) values (13);
insert into worker(id, outlet_id, name, salary)
values (13, 2, 'Артур Пендрагон', 19000);


-- Поставщики
insert into deal_party (id) values (14);
insert into storage (id) values (14);
insert into provider (id, storage_id, name, description)
values (14, 14, 'Хлебный рай', 'Продаем хлеб');

insert into deal_party (id) values (15);
insert into storage (id) values (15);
insert into provider (id, storage_id, name, description)
values (15, 15, 'Металлозавод', 'Тут изделия из металла');

insert into deal_party (id) values (16);
insert into storage (id) values (16);
insert into provider (id, storage_id, name, description)
values (16, 16, 'Очередной поставщик', 'Товары первой необходимости');

insert into deal_party (id) values (17);
insert into storage (id) values (17);
insert into provider (id, storage_id, name, description)
values (17, 17, 'Мясокомбинат', 'Мясные изделия');

insert into deal_party (id) values (18);
insert into storage (id) values (18);
insert into provider (id, storage_id, name, description)
values (18, 18, 'Рыбная ферма', 'Рыба и все такое');

insert into deal_party (id) values (19);
insert into storage (id) values (19);
insert into provider (id, storage_id, name, description)
values (19, 19, 'Мельница', 'Все что нужно пекарям');

insert into deal_party (id) values (20);
insert into storage (id) values (20);
insert into provider (id, storage_id, name, description)
values (20, 20, 'Огород на даче', 'Овощи, ягоды оптом');

insert into deal_party (id) values (21);
insert into storage (id) values (21);
insert into provider (id, storage_id, name, description)
values (21, 21, 'ООО Причал', 'Морепродукты высшего качества');

insert into deal_party (id) values (22);
insert into storage (id) values (22);
insert into provider (id, storage_id, name, description)
values (22, 22, 'Завод инструментов', 'Купите грабли');

insert into deal_party (id) values (23);
insert into storage (id) values (23);
insert into provider (id, storage_id, name, description)
values (23, 23, 'Хозтовары', 'Товары для хозяйства');

insert into deal_party (id) values (24);
insert into storage (id) values (24);
insert into provider (id, storage_id, name, description)
values (24, 24, 'Стеклозавод', 'Посуда здесь');

insert into deal_party (id) values (25);
insert into storage (id) values (25);
insert into provider (id, storage_id, name, description)
values (25, 25, 'Ферма из леса', 'Продаем мясо');

-- Товары
insert into product(id, name, quantity, unit_price)
values (1, 'Хлеб белый', 10, 20.99);
insert into storage_products(storage_id, products_id) values (14, 1);

insert into product(id, name, quantity, unit_price)
values (2, 'Хлеб бородинский', 10, 23.99);
insert into storage_products(storage_id, products_id) values (14, 2);

insert into product(id, name, quantity, unit_price)
values (3, 'Батон', 10, 28.99);
insert into storage_products(storage_id, products_id) values (14, 3);

insert into product(id, name, quantity, unit_price)
values (4, 'Булка с маком', 10, 20.99);
insert into storage_products(storage_id, products_id) values (14, 4);


insert into product(id, name, quantity, unit_price)
values (5, 'Вилка столовая алюминий 10 шт набор', 40, 20.99);
insert into storage_products(storage_id, products_id) values (15, 5);

insert into product(id, name, quantity, unit_price)
values (6, 'Вилка столовая серебро 5 шт набор', 40, 100.99);
insert into storage_products(storage_id, products_id) values (15, 6);

insert into product(id, name, quantity, unit_price)
values (7, 'Грабли', 10, 500.49);
insert into storage_products(storage_id, products_id) values (15, 7);

insert into product(id, name, quantity, unit_price)
values (8, 'Бетономешалка', 10, 4280.99);
insert into storage_products(storage_id, products_id) values (15, 8);


insert into product(id, name, quantity, unit_price)
values (9, 'Хлеб белый', 30, 18.99);
insert into storage_products(storage_id, products_id) values (16, 9);

insert into product(id, name, quantity, unit_price)
values (10, 'Мыло', 10, 23.99);
insert into storage_products(storage_id, products_id) values (16, 10);

insert into product(id, name, quantity, unit_price)
values (11, 'Веревка', 10, 98.99);
insert into storage_products(storage_id, products_id) values (16, 11);

insert into product(id, name, quantity, unit_price)
values (12, 'Соль пищевая', 40, 10.99);
insert into storage_products(storage_id, products_id) values (16, 12);


insert into product(id, name, quantity, unit_price)
values (13, 'Хлеб мясной', 10, 150.99);
insert into storage_products(storage_id, products_id) values (17, 13);

insert into product(id, name, quantity, unit_price)
values (14, 'Ветчина', 10, 323.99);
insert into storage_products(storage_id, products_id) values (17, 14);

insert into product(id, name, quantity, unit_price)
values (15, 'Говядина', 10, 270.99);
insert into storage_products(storage_id, products_id) values (17, 15);

insert into product(id, name, quantity, unit_price)
values (16, 'Сосиски', 10, 120.99);
insert into storage_products(storage_id, products_id) values (17, 16);


insert into product(id, name, quantity, unit_price)
values (17, 'Карп', 10, 180.99);
insert into storage_products(storage_id, products_id) values (18, 17);


insert into product(id, name, quantity, unit_price)
values (18, 'Хлеб из пшеницы 1 сорта', 10, 23.99);
insert into storage_products(storage_id, products_id) values (19, 18);


insert into product(id, name, quantity, unit_price)
values (19, 'Баклажан', 10, 60.99);
insert into storage_products(storage_id, products_id) values (20, 19);


insert into product(id, name, quantity, unit_price)
values (20, 'Карп высшего качества', 10, 280.99);
insert into storage_products(storage_id, products_id) values (21, 20);


insert into product(id, name, quantity, unit_price)
values (21, 'Грабли (купите)', 10, 60.99);
insert into storage_products(storage_id, products_id) values (22, 21);


insert into product(id, name, quantity, unit_price)
values (22, 'Грабли для хозяйства', 10, 120.99);
insert into storage_products(storage_id, products_id) values (23, 22);


insert into product(id, name, quantity, unit_price)
values (23, 'Тарелка для еды', 10, 120.99);
insert into storage_products(storage_id, products_id) values (24, 23);


insert into product(id, name, quantity, unit_price)
values (24, 'Говядина (из леса)', 10, 360.99);
insert into storage_products(storage_id, products_id) values (25, 24);