drop table if exists department_store;

create table department_store
(
    id int8 not null,
    halls_count int4 not null,
    floors_count int4 not null,
    primary key (id)
);

alter table if exists department_store
    add constraint fk_department_store_outlet_id foreign key (id) references outlet;