alter table if exists deal drop constraint fk_deal_deal_party_buyer_id;
alter table if exists deal drop constraint fk_deal_deal_party_seller_id;
alter table if exists deal_products drop constraint fk_deal_products_deal_deal_id;

alter table if exists deal
    add constraint fk_deal_deal_party_buyer_id foreign key (buyer_id) references deal_party on delete cascade ;
alter table if exists deal
    add constraint fk_deal_deal_party_seller_id foreign key (seller_id) references deal_party on delete cascade ;

alter table if exists deal_products
    add constraint fk_deal_products_deal_deal_id foreign key (deal_id) references deal on delete cascade ;


