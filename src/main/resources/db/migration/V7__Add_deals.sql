
-- Покупка товаров для торговой точки 1
insert into deal (id, time, buyer_id, seller_id)
values (1, '2020-06-01', 1, 14);

insert into deal (id, time, buyer_id, seller_id)
values (2, '2020-06-03', 1, 15);

insert into deal (id, time, buyer_id, seller_id)
values (3, '2020-06-01', 1, 20);

insert into deal (id, time, buyer_id, seller_id)
values (4, '2020-06-09', 1, 24);


insert into product (id, name, quantity, unit_price)
values (25, 'Хлеб белый', 10, 16.99);

insert into product (id, name, quantity, unit_price)
values (26, 'Хлеб бородинский', 10, 16.99);

insert into product (id, name, quantity, unit_price)
values (27, 'Батон', 10, 23.99);

insert into product (id, name, quantity, unit_price)
values (28, 'Крендель', 10, 24.99);

insert into product (id, name, quantity, unit_price)
values (29, 'Бублики', 10, 26.99);

insert into deal_products (deal_id, products_id)
values (1, 25), (1, 26), (1, 27), (1, 28), (1, 29);


insert into product (id, name, quantity, unit_price)
values (30, 'Вилка', 10, 40.99);

insert into product (id, name, quantity, unit_price)
values (31, 'Ложка', 10, 40.99);

insert into product (id, name, quantity, unit_price)
values (32, 'Нож', 10, 500.99);

insert into product (id, name, quantity, unit_price)
values (33, 'Сковорода', 10, 1100.99);

insert into deal_products (deal_id, products_id)
values (2, 30), (2, 31), (2, 32), (2, 33);


insert into product (id, name, quantity, unit_price)
values (34, 'Кабачок', 10, 50.99);

insert into product (id, name, quantity, unit_price)
values (35, 'Помидор', 10, 20.99);

insert into product (id, name, quantity, unit_price)
values (36, 'Тыква', 10, 100.99);

insert into deal_products (deal_id, products_id)
values (3, 34), (3, 35), (3, 36);


insert into product (id, name, quantity, unit_price)
values (37, 'Тарелка', 10, 100.99);

insert into product (id, name, quantity, unit_price)
values (38, 'Стакан', 10, 110.99);

insert into deal_products (deal_id, products_id)
values (4, 37), (4, 38);



-- Покупка товаров для торговой точки 2

insert into deal (id, time, buyer_id, seller_id)
values (5, '2020-06-02', 2, 14);

insert into deal (id, time, buyer_id, seller_id)
values (6, '2020-06-03', 2, 16);

insert into deal (id, time, buyer_id, seller_id)
values (7, '2020-06-01', 2, 18);

insert into deal (id, time, buyer_id, seller_id)
values (8, '2020-06-09', 2, 25);


insert into product (id, name, quantity, unit_price)
values (39, 'Хлеб белый', 10, 15.99);

insert into product (id, name, quantity, unit_price)
values (40, 'Батон', 10, 20.99);

insert into deal_products (deal_id, products_id)
values (5, 39), (5, 40);


insert into product (id, name, quantity, unit_price)
values (41, 'Мыло', 10, 30.99);

insert into deal_products (deal_id, products_id)
values (6, 41);


insert into product (id, name, quantity, unit_price)
values (42, 'Лосось', 10, 273.99);

insert into product (id, name, quantity, unit_price)
values (43, 'Щука', 10, 150.99);

insert into deal_products (deal_id, products_id)
values (7, 42), (7, 43);


insert into product (id, name, quantity, unit_price)
values (44, 'Говядина', 10, 145.99);

insert into product (id, name, quantity, unit_price)
values (45, 'Оленина', 10, 320.99);

insert into deal_products (deal_id, products_id)
values (8, 44), (8, 45);



-- Покупка товаров для торговой точки 3

insert into deal (id, time, buyer_id, seller_id)
values (9, '2020-05-29', 3, 15);

insert into deal (id, time, buyer_id, seller_id)
values (10, '2020-06-04', 3, 17);

insert into deal (id, time, buyer_id, seller_id)
values (11, '2020-05-15', 3, 21);

insert into deal (id, time, buyer_id, seller_id)
values (12, '2020-06-04', 3, 22);


insert into product (id, name, quantity, unit_price)
values (46, 'Кастрюля', 10, 420.99);

insert into product (id, name, quantity, unit_price)
values (47, 'Ложка', 10, 20.99);

insert into deal_products (deal_id, products_id)
values (9, 46), (9, 47);


insert into product (id, name, quantity, unit_price)
values (48, 'Фарш куриный', 10, 120.99);

insert into product (id, name, quantity, unit_price)
values (49, 'Фарш свиной', 10, 220.99);

insert into deal_products (deal_id, products_id)
values (10, 48), (10, 49);


insert into product (id, name, quantity, unit_price)
values (50, 'Щука', 10, 120.99);

insert into product (id, name, quantity, unit_price)
values (51, 'Форель', 10, 310.99);

insert into deal_products (deal_id, products_id)
values (11, 50), (11, 51);


insert into product (id, name, quantity, unit_price)
values (52, 'Грабли', 10, 410.99);

insert into product (id, name, quantity, unit_price)
values (53, 'Бетономешалка', 3, 5500.99);

insert into deal_products (deal_id, products_id)
values (12, 52), (12, 53);


-- Покупка товаров для торговой точки 4

insert into deal (id, time, buyer_id, seller_id)
values (13, '2020-06-01', 4, 15);

insert into deal (id, time, buyer_id, seller_id)
values (14, '2020-06-03', 4, 19);

insert into deal (id, time, buyer_id, seller_id)
values (15, '2020-06-01', 4, 23);


insert into product (id, name, quantity, unit_price)
values (54, 'Кастрюля', 3, 420.99);

insert into product (id, name, quantity, unit_price)
values (55, 'Ложка', 10, 20.99);

insert into deal_products (deal_id, products_id)
values (13, 54), (13, 55);


insert into product (id, name, quantity, unit_price)
values (56, 'Мука пшеничная', 10, 30.79);

insert into product (id, name, quantity, unit_price)
values (57, 'Крахмал кукурузный', 10, 80.99);

insert into deal_products (deal_id, products_id)
values (14, 56), (14, 57);


insert into product (id, name, quantity, unit_price)
values (58, 'Грабли', 5, 320.79);

insert into product (id, name, quantity, unit_price)
values (59, 'Кастрюля', 5, 120.99);

insert into deal_products (deal_id, products_id)
values (15, 58), (15, 59);



-- Покупка товаров для торговой точки 5


insert into deal (id, time, buyer_id, seller_id)
values (17, '2020-06-01', 5, 14);

insert into deal (id, time, buyer_id, seller_id)
values (18, '2020-06-03', 5, 15);


insert into product (id, name, quantity, unit_price)
values (60, 'Хлеб белый', 5, 16.99);

insert into product (id, name, quantity, unit_price)
values (61, 'Хлеб бородинский', 5, 17.99);

insert into deal_products (deal_id, products_id)
values (17, 60), (17, 61);


insert into product (id, name, quantity, unit_price)
values (62, 'Вилка', 5, 26.99);

insert into product (id, name, quantity, unit_price)
values (63, 'Нож', 5, 60.99);

insert into deal_products (deal_id, products_id)
values (18, 62), (18, 63);



-- Теперь продажи из торговых точек

insert into deal_party(id)
values (26), (27), (28), (29), (30), (31), (32), (33), (34), (35);

insert into customer (id, name)
values (26, 'Крючков Демьян'),
       (27, 'Тукаев Марк'),
       (28, 'Тоболенко Нестор'),
       (29, 'Вольпов Игнат'),
       (30, 'Волков Святослав'),
       (31, 'Цыганкова Валерия'),
       (32, 'Хрущёва Вероника'),
       (33, 'Сухорукова Рената'),
       (34, 'Щербинина Мирослава'),
       (35, 'Букавицкая Майя');

insert into deal (id, time, buyer_id, seller_id)
values (19, '2020-06-03', 26, 6);


insert into product (id, name, quantity, unit_price)
values (64, 'Хлеб белый', 1, 16.99);

insert into product (id, name, quantity, unit_price)
values (65, 'Батон', 1, 23.99);

insert into product (id, name, quantity, unit_price)
values (66, 'Бублики', 1, 26.99);

insert into product (id, name, quantity, unit_price)
values (67, 'Вилка', 1, 40.99);

insert into product (id, name, quantity, unit_price)
values (68, 'Нож', 1, 500.99);

insert into product (id, name, quantity, unit_price)
values (69, 'Кабачок', 1, 50.99);

insert into product (id, name, quantity, unit_price)
values (70, 'Помидор', 4, 20.99);

insert into product (id, name, quantity, unit_price)
values (71, 'Тарелка', 1, 100.99);

insert into deal_products (deal_id, products_id)
values (19, 64), (19, 65), (19, 66), (19, 67), (19, 68), (19, 69), (19, 70), (19, 71);


insert into deal (id, time, buyer_id, seller_id)
values (20, '2020-06-03', 28, 7);


insert into product (id, name, quantity, unit_price)
values (72, 'Хлеб белый', 3, 15.99);

insert into product (id, name, quantity, unit_price)
values (73, 'Батон', 3, 20.99);

insert into product (id, name, quantity, unit_price)
values (74, 'Мыло', 3, 30.99);

insert into product (id, name, quantity, unit_price)
values (75, 'Лосось', 2, 273.99);

insert into product (id, name, quantity, unit_price)
values (76, 'Щука', 4, 150.99);

insert into product (id, name, quantity, unit_price)
values (77, 'Говядина', 3, 145.99);

insert into deal_products (deal_id, products_id)
values (20, 72), (20, 73), (20, 74), (20, 75), (20, 76), (20, 77);


insert into deal (id, time, buyer_id, seller_id)
values (21, '2020-06-03', 30, 8);

insert into product (id, name, quantity, unit_price)
values (78, 'Кастрюля', 2, 420.99);

insert into product (id, name, quantity, unit_price)
values (79, 'Ложка', 4, 20.99);

insert into product (id, name, quantity, unit_price)
values (80, 'Фарш свиной', 3, 220.99);

insert into product (id, name, quantity, unit_price)
values (81, 'Форель', 2, 310.99);

insert into product (id, name, quantity, unit_price)
values (82, 'Грабли', 2, 410.99);

insert into product (id, name, quantity, unit_price)
values (83, 'Бетономешалка', 1, 5500.99);

insert into deal_products (deal_id, products_id)
values (21, 78), (21, 79), (21, 80), (21, 81), (21, 82), (21, 83);


insert into deal (id, time, buyer_id, seller_id)
values (22, '2020-06-03', 32, 9);

insert into product (id, name, quantity, unit_price)
values (84, 'Кастрюля', 2, 420.99);

insert into product (id, name, quantity, unit_price)
values (85, 'Ложка', 4, 20.99);

insert into product (id, name, quantity, unit_price)
values (86, 'Мука пшеничная', 2, 60.79);

insert into product (id, name, quantity, unit_price)
values (87, 'Крахмал кукурузный', 2, 100.99);

insert into product (id, name, quantity, unit_price)
values (88, 'Грабли', 1, 320.79);

insert into deal_products (deal_id, products_id)
values (22, 84), (22, 85), (22, 86), (22, 87), (22, 88);


insert into deal (id, time, buyer_id, seller_id)
values (23, '2020-06-03', 34, 10);

insert into product (id, name, quantity, unit_price)
values (89, 'Хлеб белый', 2, 28.99);

insert into product (id, name, quantity, unit_price)
values (90, 'Хлеб бородинский', 1, 23.99);

insert into product (id, name, quantity, unit_price)
values (91, 'Вилка', 2, 46.99);

insert into product (id, name, quantity, unit_price)
values (92, 'Нож', 1, 100.99);

insert into deal_products (deal_id, products_id)
values (23, 89), (23, 90), (23, 91), (23, 92);


drop sequence if exists hibernate_sequence;

create sequence hibernate_sequence start 100 increment 1;