insert into product (id, name, quantity, unit_price)
values (93, 'Хлеб белый', 10, 20.99);

insert into product (id, name, quantity, unit_price)
values (94, 'Хлеб бородинский', 10, 22.99);

insert into product (id, name, quantity, unit_price)
values (95, 'Батон', 10, 23.99);

insert into product (id, name, quantity, unit_price)
values (96, 'Крендель', 10, 24.99);

insert into product (id, name, quantity, unit_price)
values (97, 'Бублики', 10, 26.99);

insert into storage_products (storage_id, products_id)
values (1, 93), (1, 94), (1, 95), (1, 96), (1, 97);


insert into product (id, name, quantity, unit_price)
values (98, 'Вилка', 10, 40.99);

insert into product (id, name, quantity, unit_price)
values (99, 'Ложка', 10, 40.99);

insert into product (id, name, quantity, unit_price)
values (100, 'Нож', 10, 500.99);

insert into product (id, name, quantity, unit_price)
values (101, 'Сковорода', 10, 1100.99);

insert into storage_products (storage_id, products_id)
values (1, 98), (1, 99), (1, 100), (1, 101);


insert into product (id, name, quantity, unit_price)
values (102, 'Кабачок', 10, 50.99);

insert into product (id, name, quantity, unit_price)
values (103, 'Помидор', 10, 20.99);

insert into product (id, name, quantity, unit_price)
values (104, 'Тыква', 10, 100.99);

insert into storage_products (storage_id, products_id)
values (1, 102), (1, 103), (1, 104);


insert into product (id, name, quantity, unit_price)
values (105, 'Тарелка', 10, 100.99);

insert into product (id, name, quantity, unit_price)
values (106, 'Стакан', 10, 110.99);

insert into storage_products (storage_id, products_id)
values (1, 105), (1, 106);



insert into product (id, name, quantity, unit_price)
values (107, 'Батон', 10, 20.99);

insert into storage_products (storage_id, products_id)
values (2, 107);


insert into product (id, name, quantity, unit_price)
values (108, 'Мыло', 10, 30.99);

insert into storage_products (storage_id, products_id)
values (2, 108);


insert into product (id, name, quantity, unit_price)
values (109, 'Лосось', 10, 273.99);

insert into product (id, name, quantity, unit_price)
values (110, 'Щука', 10, 150.99);

insert into storage_products (storage_id, products_id)
values (2, 109), (2, 110);


insert into product (id, name, quantity, unit_price)
values (111, 'Говядина', 10, 145.99);

insert into product (id, name, quantity, unit_price)
values (112, 'Оленина', 10, 320.99);

insert into storage_products (storage_id, products_id)
values (2, 111), (2, 112);



insert into product (id, name, quantity, unit_price)
values (113, 'Кастрюля', 10, 420.99);

insert into product (id, name, quantity, unit_price)
values (114, 'Ложка', 10, 20.99);

insert into storage_products (storage_id, products_id)
values (3, 113), (3, 114);


insert into product (id, name, quantity, unit_price)
values (115, 'Фарш куриный', 10, 120.99);

insert into product (id, name, quantity, unit_price)
values (116, 'Фарш свиной', 10, 220.99);

insert into storage_products (storage_id, products_id)
values (3, 115), (3, 116);


insert into product (id, name, quantity, unit_price)
values (117, 'Щука', 10, 120.99);

insert into product (id, name, quantity, unit_price)
values (118, 'Форель', 10, 310.99);

insert into storage_products (storage_id, products_id)
values (3, 117), (3, 118);


insert into product (id, name, quantity, unit_price)
values (119, 'Грабли', 10, 410.99);

insert into product (id, name, quantity, unit_price)
values (120, 'Бетономешалка', 3, 5500.99);

insert into storage_products (storage_id, products_id)
values (4, 119), (4, 120);

insert into product (id, name, quantity, unit_price)
values (121, 'Кастрюля', 3, 420.99);

insert into product (id, name, quantity, unit_price)
values (122, 'Ложка', 10, 20.99);

insert into storage_products (storage_id, products_id)
values (4, 121), (4, 122);


insert into product (id, name, quantity, unit_price)
values (123, 'Мука пшеничная', 10, 30.79);

insert into product (id, name, quantity, unit_price)
values (124, 'Крахмал кукурузный', 10, 80.99);

insert into storage_products (storage_id, products_id)
values (5, 123), (5, 124);


insert into product (id, name, quantity, unit_price)
values (125, 'Грабли', 5, 320.79);

insert into storage_products (storage_id, products_id)
values (5, 125);

drop sequence if exists hibernate_sequence;

create sequence hibernate_sequence start 126 increment 1;