import Vue from 'vue'

const outlets = Vue.resource('/outlet{/id}');

export default {
    getById: id => outlets.get({id}),
    getOutletsList: () => outlets.get()
}