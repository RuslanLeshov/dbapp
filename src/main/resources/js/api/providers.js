import Vue from 'vue'

const providers = Vue.resource('/provider{/id}');

export default {
    getAll: () => providers.get()
}