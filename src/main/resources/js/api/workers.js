import Vue from 'vue'

const workers = Vue.resource('/worker{/id}');

export default {
    getAll: () => workers.get(),
    add: worker => workers.save({}, worker),
    update: worker => workers.update({id: worker.id}, worker),
    remove: id => workers.remove({id})
}