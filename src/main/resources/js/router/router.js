import Vue from 'vue'
import VueRouter from 'vue-router'
import OutletsList from 'pages/OutletsList.vue'
import WorkersPage from 'pages/WorkersPage.vue'
import ProviderList from 'pages/ProviderList.vue'
import ProductList from 'pages/ProductList.vue'
import DealsPage from 'pages/DealsPage.vue'
import CustomersPage from 'pages/CustomersPage.vue'

Vue.use(VueRouter);

const routes = [
    { path: '/w/:id', component: WorkersPage},
    { path: '/p', component: ProviderList, props: (route) => ({size: route.query.size, page: route.query.page})},
    { path: '/', component: OutletsList },
    { path: '/prod/:id', component: ProductList, props: (route) => ({ownerType: route.query.ownerType})},
    { path: '/d', component: DealsPage},
    { path: '/c', component: CustomersPage}
];

export default new VueRouter({
    mode: 'history',
    routes: routes
});