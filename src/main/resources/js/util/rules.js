export default {
    nameRules: [
        v => !!v || 'Name is required',
        v => v.length <= 60 || 'Name must be less than 60 characters'
    ],
    moneyRules: [
        v => !!v || 'This field is required',
        v => v > 0 || 'Value must be positive'
    ]
}