CREATE OR REPLACE FUNCTION count_workers(oid outlet.id%TYPE)
    RETURNS int8 AS
$BODY$
BEGIN
    return (SELECT COUNT(*) FROM worker WHERE oid = worker.outlet_id);
END;
$BODY$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_workers_limit(oid outlet.id%TYPE)
    RETURNS int8 AS
$BODY$
BEGIN
    return (SELECT workplaces_count FROM outlet WHERE oid = id);
END;
$BODY$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_workers_limit()
    RETURNS trigger AS
$BODY$
BEGIN
    IF count_workers(NEW.outlet_id) = get_workers_limit(NEW.outlet_id) THEN
        RAISE EXCEPTION 'Превышен лимит сотрудников торговой точки %', NEW.outlet_id;
    end if;
    RETURN NEW;
END
$BODY$
    LANGUAGE plpgsql;

CREATE TRIGGER check_workers_limit_trigger
    BEFORE INSERT
    ON worker
    FOR EACH ROW
EXECUTE PROCEDURE check_workers_limit();


CREATE OR REPLACE FUNCTION get_storage_for_owner_id(owner_id deal_party.id%TYPE)
    RETURNS storage.id%TYPE AS
$BODY$
BEGIN
    RETURN (SELECT storage.id
            FROM storage
                     INNER JOIN outlet o on storage.id = o.storage_id AND o.id = owner_id
            UNION
            SELECT storage.id
            FROM storage
                     INNER JOIN provider p on storage.id = p.storage_id AND p.id = owner_id);

END;
$BODY$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_products_for_deal(required_deal_id deal.id%TYPE)
    RETURNS TABLE
            (
                id         product.id%TYPE,
                name       product.name%TYPE,
                quantity   product.quantity%TYPE,
                unit_price product.unit_price%TYPE
            )
AS
$BODY$
BEGIN
    RETURN QUERY
        SELECT product.id, product.name, product.quantity
        FROM product
                 INNER JOIN deal_products dp on product.id = dp.products_id AND dp.deal_id = required_deal_id;
END;
$BODY$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_product_in_storage(s_id storage.id%TYPE, p_name product.name%TYPE)
    RETURNS TABLE
            (
                id       product.id%TYPE,
                quantity product.id%TYPE
            )
AS
$BODY$
BEGIN
    return QUERY
        SELECT product.id, product.quantity
        FROM product
                 INNER JOIN storage_products sp on product.id = sp.products_id AND sp.storage_id = s_id
        WHERE product.name = p_name;
END;
$BODY$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_products_for_deal_cursor(required_deal_id deal.id%TYPE)
    RETURNS refcursor
AS
$BODY$
DECLARE
    the_cursor refcursor;
BEGIN
    OPEN the_cursor FOR
        SELECT product.id, product.name, product.quantity
        FROM product
                 INNER JOIN deal_products dp on product.id = dp.products_id AND dp.deal_id = required_deal_id;
    RETURN the_cursor;
END;
$BODY$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_product_in_storage_cursor(s_id storage.id%TYPE, p_name product.name%TYPE)
    RETURNS refcursor
AS
$BODY$
DECLARE
    the_cursor refcursor;
BEGIN
    OPEN the_cursor FOR
        SELECT product.id, product.quantity
        FROM product
                 INNER JOIN storage_products sp on product.id = sp.products_id AND sp.storage_id = s_id
        WHERE product.name = p_name;
    RETURN the_cursor;
END;
$BODY$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION update_storage_for_deal()
    RETURNS trigger AS
$BODY$
DECLARE
    products_in_deal refcursor;
    target record;
    seller_storage     storage.id%TYPE := get_storage_for_owner_id(NEW.seller_id);
    buyer_storage      storage.id%TYPE := get_storage_for_owner_id(NEW.buyer_id);

    product_in_storage record;
--         TABLE
--                        (
--                            id       product.id%TYPE,
--                            quantity product.id%TYPE
--                        );
    id_val             product.id%TYPE;
BEGIN

    products_in_deal := get_products_for_deal_cursor(NEW.id);



        LOOP
            FETCH products_in_deal INTO target;

            product_in_storage := get_product_in_storage(seller_storage, target.name);

            IF product_in_storage.quantity < target.quantity THEN
                RAISE EXCEPTION 'Попытка купить больше чем есть из склада %, сделка %', seller_storage, NEW.id;
            END IF;

            UPDATE product
            SET quantity = quantity - target.quantity
            WHERE product.id = product_in_storage.id;

            product_in_storage := get_product_in_storage(buyer_storage, target.name);
            IF EXISTS(product_in_storage) THEN
                UPDATE product
                SET quantity = quantity + target.quantity
                WHERE product.id = product_in_storage.id;
            ELSE
                id_val = (SELECT nextval('hibernate_sequence'));
                INSERT INTO product VALUES (id_val, target.name, target.quantity, target.unit_price);
                INSERT INTO storage_products VALUES (buyer_storage, id_val);
            END IF;
        END LOOP;
END;
$BODY$
    LANGUAGE plpgsql;

CREATE TRIGGER update_storage_for_deal_trigger
    BEFORE INSERT
    ON deal
    FOR EACH ROW
EXECUTE PROCEDURE update_storage_for_deal();




